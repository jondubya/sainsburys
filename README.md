Run from directory

To run tests
>mvn clean test

To create jar
>mvn clean package

To run application 
(with hardcoded, ripe fruits html, would move this into a property file)
>mvn exec:java -Dexec.mainClass="com.sainsburys.grocery.coding_assignment.GroceryScanner"

with alternative grocery page
mvn exec:java -Dexec.mainClass="com.sainsburys.grocery.coding_assignment.GroceryScanner" -Dexec.args="http://www.sainsburys.co.uk/shop/gb/groceries/******/******/****"

e.g for bananas and grapes
mvn exec:java -Dexec.mainClass="com.sainsburys.grocery.coding_assignment.GroceryScanner" -Dexec.args="http://www.sainsburys.co.uk/shop/gb/groceries/fruit-veg/bananas-grapes#langId=44&storeId=10151&catalogId=10122&categoryId=12551&parent_category_rn=12518&top_category=12518&pageSize=30&orderBy=FAVOURITES_FIRST&searchTerm=&beginIndex=0"