package com.sainsburys.grocery.coding_assignment;

import com.sainsburys.grocery.coding_assignment.crawler.FruitProductCrawler;
import com.sainsburys.grocery.coding_assignment.crawler.ProductCrawler;
import com.sainsburys.grocery.coding_assignment.display.ProductListOutputer;

public class GroceryScanner 
{
	public static void main(String[] args) {
		String baseUrl = args.length != 0 ? args[0] : "http://www.sainsburys.co.uk/shop/gb/groceries/fruit-veg/ripe---ready#langId=44&storeId=10151&catalogId=10122&categoryId=185749&parent_category_rn=12518&top_category=12518&pageSize=30&orderBy=FAVOURITES_FIRST&searchTerm=&beginIndex=0";
    	
		ProductCrawler fruitProductCrawler = new FruitProductCrawler(baseUrl);
		fruitProductCrawler.generateProductInformation();
		
		ProductListOutputer productListOutputer = new ProductListOutputer(fruitProductCrawler.getProductList());
		System.out.println(productListOutputer.toJson());
	}
}
