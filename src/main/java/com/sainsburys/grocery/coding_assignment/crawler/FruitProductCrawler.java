package com.sainsburys.grocery.coding_assignment.crawler;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.sainsburys.grocery.coding_assignment.domain.Product;
import com.sainsburys.grocery.coding_assignment.stream.InputStreamCache;
import com.sainsburys.grocery.coding_assignment.stream.InputStreamCacheBuilder;
import com.sainsburys.grocery.coding_assignment.stream.LinkInformationGetter;

public class FruitProductCrawler extends ProductCrawler {

	InputStreamCacheBuilder inputStreamCacheBuilder = new InputStreamCacheBuilder();
	LinkInformationGetter linkInformationGetter = new LinkInformationGetter();

	public FruitProductCrawler(String baseUrl) {
		super(baseUrl);
	}

	@Override
	protected void getBaseURLInformation() throws ParseException, IOException {
		Document doc = Jsoup.connect(baseUrl).userAgent(USER_AGENT).get();
		Elements products = doc.getElementsByClass("product");

		String title = null;
		String link = null;
		String price = null;

		for (Element product : products) {

			title = product.getElementsByClass("productNameAndPromotions").select("a").text();
			link = product.getElementsByClass("productNameAndPromotions").select("a").attr("href");
			price = product.getElementsByClass("pricePerUnit").text();

			Product p = new Product(title, parsePriceStringToDouble(price),
					link);
			// System.out.println(p);
			productList.add(p);
		}
	}

	@Override
	protected void getLinkURLInformation() throws IOException {

		for (Product product : productList) {
			getLinkData(product);
		}
	}

	/**
	 * Fills in the data that requires opening up a connection to the new page
	 * [pageSize, description]
	 * 
	 * @param product
	 * @throws IOException
	 */
	public void getLinkData(Product product) throws IOException {

		String address = product.getLink();

		InputStreamCache inputStreamCache = inputStreamCacheBuilder.generateInputStreamFromURL(address, USER_AGENT);

		// Fill in blank description
		String desc = linkInformationGetter.getDescription(inputStreamCache.getInputStream());
		product.setDescription(desc);

		// Find the size of file
		long length = linkInformationGetter.getContentLength(inputStreamCache.getInputStream());
		product.setSize(length);
		// System.out.println("length " + length);
	}

	protected double parsePriceStringToDouble(String price) throws ParseException {
		String parsePrice = price.split("/")[0];
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.UK);
		double unitPrice = numberFormat.parse(parsePrice).doubleValue();
		return unitPrice;
	} 
	
	public void setLinkInformationGetter(LinkInformationGetter linkInformationGetter) {
		this.linkInformationGetter = linkInformationGetter;
	}
	
	public void setInputStreamCacheBuilder(InputStreamCacheBuilder inputStreamCacheBuilder) {
		this.inputStreamCacheBuilder = inputStreamCacheBuilder;
	}


}
