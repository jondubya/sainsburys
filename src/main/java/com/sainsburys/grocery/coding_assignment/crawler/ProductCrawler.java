package com.sainsburys.grocery.coding_assignment.crawler;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.sainsburys.grocery.coding_assignment.domain.Product;

public abstract class ProductCrawler {

	protected String baseUrl;
	protected static final String USER_AGENT = "Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6";
	protected List<Product> productList = new ArrayList<>();
	
	public ProductCrawler(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public List<Product> getProductList() {
		return productList;
	}
	
	public void generateProductInformation() {
		try {
			getBaseURLInformation();
			getLinkURLInformation();
		} catch (ParseException | IOException e) {
			e.printStackTrace();
		}
	}

	protected abstract void getBaseURLInformation() throws ParseException, IOException;
	protected abstract void getLinkURLInformation() throws IOException;
	
}
