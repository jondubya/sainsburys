package com.sainsburys.grocery.coding_assignment.display;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sainsburys.grocery.coding_assignment.domain.Product;

/**
 * 
 * DisplayWrapper for a list of objects and the total cost of each unit
 *
 */
public class ProductListOutputer {

	private List<Product> results;

	public ProductListOutputer() {
		results = new ArrayList<>();
	}
	
	public ProductListOutputer(List<Product> results) {
		this.results = results;
	}
	
	public void addProduct(Product product) {
		results.add(product);
	}

	public List<Product> getResults() {
		return results;
	}
	
	void setResults(List<Product> results) {
		this.results = results;
	}
	
	public double getTotal() {
		double accumulatedTotal = 0;
		for (Product product : results) {
			accumulatedTotal += product.getUnitPrice();
		}
		return accumulatedTotal;
	}

	public String toJson() {
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {
			jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonInString;
	}
}
