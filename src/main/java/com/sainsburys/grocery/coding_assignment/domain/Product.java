package com.sainsburys.grocery.coding_assignment.domain;

import java.text.DecimalFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {
	
	private static DecimalFormat df = new DecimalFormat(".##");
	private String link;
	private String title;
	private long size;
	private double unitPrice;
	private String description;
	
	public Product(String title,  double unitPrice, String link) {
		this.title = title;
		this.link = link;
		this.unitPrice = unitPrice;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@JsonIgnore
	public String getLink() {
		return link;
	}
	
	/*
	 * Public getter methods used by Jackson to display attributes, would probably 
	 * prefer to use a wrapper class and leave this strictly as a POJO
	 */
	
	public String getTitle() {
		return title;
	}

	public void setSize(long size) {
		this.size = size;
	}
	
	@JsonProperty("size")
	public String getDisplaySize() {
		return df.format(size/1000) + "kb";
	}
	
	public String getDescription() {
		return description;
	}
	
	public double getUnitPrice() {
		return unitPrice;
	}
	
	@Override
	public String toString() {
		return "Result [title=" + title + ", link=" + link +", unitPrice=" + unitPrice+ ", size=" + size 
				+ ", description=" + description + "]";
	}
	

}
