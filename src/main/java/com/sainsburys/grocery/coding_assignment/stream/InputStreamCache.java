package com.sainsburys.grocery.coding_assignment.stream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

/**
 * Class created so an input stream didn't have to be opened up twice
 * @author jwong
 *
 */
public class InputStreamCache {

	private byte[] bytes;
	
	public InputStreamCache(InputStream input) {
		try {
			bytes = IOUtils.toByteArray(input);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} finally {
			IOUtils.closeQuietly(input);
		}
	}
	
	public InputStream getInputStream() {
		return new ByteArrayInputStream(bytes);
	}
}
