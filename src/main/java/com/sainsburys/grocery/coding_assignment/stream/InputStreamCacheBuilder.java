package com.sainsburys.grocery.coding_assignment.stream;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class InputStreamCacheBuilder {

	public InputStreamCache generateInputStreamFromURL(String address, String userAgent)
			throws MalformedURLException, IOException, ClientProtocolException {
		URL url = new URL(address);
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = null;
		try {
			request = new HttpGet(url.toURI());
			request.addHeader("User-Agent", userAgent);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		}
		HttpResponse response = client.execute(request);

		InputStreamCache inputStreamCache = new InputStreamCache(response.getEntity().getContent());
		return inputStreamCache;
	}
}
