package com.sainsburys.grocery.coding_assignment.stream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.CountingInputStream;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class LinkInformationGetter {

	/**
	 * Given an InputStream, returns the content length, currently used to
	 * measure the size of an html page is without external links
	 * 
	 * @param inputStream
	 * @return file size in bytes
	 */
	public long getContentLength(InputStream inputStream) {
		CountingInputStream counter = null;
		try {
			counter = new CountingInputStream(inputStream);
			IOUtils.toString(counter);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {
			IOUtils.closeQuietly(counter);
		}
		return counter.getByteCount();
	}
	
	/**
	 * Given an InputStream, (which in the current case is a clicked linked
	 * page), this parses the stream and returns the product description within
	 * the page
	 * 
	 * @param inputStream
	 * @return
	 * @throws IOException
	 */
	public String getDescription(InputStream inputStream) throws IOException {
		StringBuilder content = new StringBuilder();
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

		String line;

		while ((line = bufferedReader.readLine()) != null) {
			content.append(line + "\n");
		}
		bufferedReader.close();

		Document doc = Jsoup.parse(content.toString());
		// System.out.println("description : " +
		// doc.getElementsByClass("productText").get(0).text());

		return doc.getElementsByClass("productText").get(0).text();
	}
}
