package com.sainsburys.grocery.coding_assignment.crawler;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;

import com.sainsburys.grocery.coding_assignment.domain.Product;
import com.sainsburys.grocery.coding_assignment.stream.InputStreamCache;
import com.sainsburys.grocery.coding_assignment.stream.InputStreamCacheBuilder;
import com.sainsburys.grocery.coding_assignment.stream.LinkInformationGetter;

public class FruitProductCrawlerTest {

	@Test
	public void testGetLinkDataFromInputStream() throws IOException {
		FruitProductCrawler productCrawler = new FruitProductCrawler(null);
		productCrawler.setLinkInformationGetter(new MockLinkInfoGetter());
		productCrawler.setInputStreamCacheBuilder(new MockInputStreamCacheBuilder());
		Product testProduct = new Product(null, 0, null);
		productCrawler.getLinkData(testProduct);
		
		assertEquals("123.0kb", testProduct.getDisplaySize());
		assertEquals("Mock info description", testProduct.getDescription());
	}
	
	class MockLinkInfoGetter extends LinkInformationGetter {
		@Override
		public long getContentLength(InputStream inputStream) {
			return 123456;
		}
		@Override
		public String getDescription(InputStream inputStream) throws IOException {
			return "Mock info description";
		}
	}

	class MockInputStreamCacheBuilder extends InputStreamCacheBuilder {
		@Override
		public InputStreamCache generateInputStreamFromURL(String address,String userAgent) throws MalformedURLException, IOException,
				ClientProtocolException {
			return new InputStreamCache(new ByteArrayInputStream("foo".getBytes("UTF-8")));
		}
	}
}
