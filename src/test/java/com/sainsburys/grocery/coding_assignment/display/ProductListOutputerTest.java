package com.sainsburys.grocery.coding_assignment.display;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sainsburys.grocery.coding_assignment.domain.Product;

public class ProductListOutputerTest {

	@Test
	public void testCalculateTotalPrice() {
		Product productA = new Product(null, 1.23, null);
		Product productB = new Product(null, 2.54, null);
		Product productC = new Product(null, 3.79, null);

		ProductListOutputer pdc = new ProductListOutputer();
		pdc.addProduct(productA);
		pdc.addProduct(productB);
		pdc.addProduct(productC);
		
		Assert.assertEquals(7.56, pdc.getTotal(), 0.001);
	}
	
	@Test
	public void testToJson() throws JsonProcessingException, IOException {
		Product productA = new Product("titleA", 3.50, "http://www.somwhere.com");
		productA.setSize(34456);
		productA.setDescription("Product A description");
		Product productB = new Product("titleB", 1.50, "http://www.somwhereelse.com");
		productB.setSize(64323);
		productB.setDescription("Product B description");

		ProductListOutputer pdc = new ProductListOutputer();
		pdc.addProduct(productA);
		pdc.addProduct(productB);

		String expectedJsonString = "{ \"results\" : [ {" + " \"title\" : \"titleA\"," + "\"size\" : \"34.0kb\","
				+ "\"unitPrice\" : 3.5," + " \"description\" : \"Product A description\"" + " }, {"
				+ "  \"title\" : \"titleB\"," + "  \"size\" : \"64.0kb\"," + "  \"unitPrice\" : 1.5,"
				+ "  \"description\" : \"Product B description\"" + " } ]," + " \"total\" : 5.0" + " }";
		//compare expected string and that returned by ProductPageContainer, after removing whitespace and line carraiges
		Assert.assertEquals(expectedJsonString.replace(" ", ""), pdc.toJson().replace(" ", "").replace("\n", ""));
	}

}
