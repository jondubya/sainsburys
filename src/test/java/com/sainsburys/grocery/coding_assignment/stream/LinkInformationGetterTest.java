package com.sainsburys.grocery.coding_assignment.stream;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import com.sainsburys.grocery.coding_assignment.crawler.FruitProductCrawler;

public class LinkInformationGetterTest {

	
	@Test
	public void testGetDescription() throws IOException {
		
        LinkInformationGetter linkInfoGetter = new LinkInformationGetter();
    	ClassLoader classloader = Thread.currentThread().getContextClassLoader();
    	InputStream localInputStream = classloader.getResource("grape1.html").openStream();
    	
    	String desc = linkInfoGetter.getDescription(localInputStream);
    	assertEquals("TTD Grapes", desc);
	}
	
	@Test
	public void testGetContentLength() throws Exception {
		
		LinkInformationGetter linkInfoGetter = new LinkInformationGetter();
    	ClassLoader classloader = Thread.currentThread().getContextClassLoader();
    	InputStream localInputStream = classloader.getResource("grape1.html").openStream();
    	
        long size = linkInfoGetter.getContentLength(localInputStream);
        //41438 is the file size given through explorer, or right clicking in eclipse -> properties -> Size
        assertEquals(41438, size, 0);
       
	}
}
